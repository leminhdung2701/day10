<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="css.css" type="text/css">

<div>
    <h1>Kết quả</h1>
</div>

<?php
$questions = array(
    1 => "Xe máy nặng bao nhiêu cân?",
    2 => "Năm nay năm bao nhiêu?",
    3 => "Bạn Duy thích học môn nào nhất?",
    4 => "Bạn Duy nghiện gì?",
    5 => "Khi nào K64 tốt nghiệp?",

    6 => "Xe máy nặng bao nhiêu cân?",
    7 => "Năm nay năm bao nhiêu?",
    8 => "Bạn Duy thích học môn nào nhất?",
    9 => "Bạn Duy nghiện gì?",
    10 => "Khi nào K64 tốt nghiệp?",
);

$answers = array(
    array("a" => "100 kg", "b" => "10 yến", "c" => "1 tạ", "d" => "0,1 tấn"),
    array("a" => "2010", "b" => "2022", "c" => "2023", "d" => "2011"),
    array("a" => "Lập trình Web", "b" => "Học máy", "c" => "Thị giác máy tính", "d" => "Giải tích số"),
    array("a" => "Bùng học", "b" => "LOL", "c" => "CSGO", "d" => "Dota2"),
    array("a" => "01/2023", "b" => "12/2022", "c" => "01/2024", "d" => "08/2023"),

    array("a" => "100 kg", "b" => "10 yến", "c" => "1 tạ", "d" => "0,1 tấn"),
    array("a" => "2010", "b" => "2022", "c" => "2023", "d" => "2011"),
    array("a" => "Lập trình Web", "b" => "Học máy", "c" => "Thị giác máy tính", "d" => "Giải tích số"),
    array("a" => "Bùng học", "b" => "LOL", "c" => "CSGO", "d" => "Dota2"),
    array("a" => "01/2023", "b" => "12/2022", "c" => "01/2024", "d" => "08/2023"),
);

$keys = array(
    1 => "a",
    2 => "b",
    3 => "a",
    4 => "c",
    5 => "d",

    6 => "a",
    7 => "b",
    8 => "a",
    9 => "c",
    10 => "d",
);

$point = 0;
$results = array();

if(isset($_COOKIE['cookie_ans_1'])&& isset($_COOKIE['cookie_ans_2'])) {
    $results_1 =  json_decode($_COOKIE['cookie_ans_1'], true);
    $results_2 =  json_decode($_COOKIE['cookie_ans_2'], true);
    $results = array_merge($results_1,$results_2);
    echo '<div><h3>' . "Đáp án của bạn: " . '</div>';

    foreach ($questions as $question => $value) {
        echo
        '<fieldset>
        <legend>Câu ' . $question . ':</legend>
        <div>
                ' . $value . '
        </div>';
        foreach ($answers[$question - 1] as $answer => $value_ans) {
            if ($results[$question -1] == $keys[$question] && $answer == $results[$question -1]) {
                echo
                '<div style="color:green">
                    <input type="radio" id="" name="' . $question . '" value="' . $answer . '" checked>
                    <label for="">' . $value_ans . '</label>
                </div>';
                $point += 1;
            }else if($results[$question -1] != $keys[$question] && $answer == $results[$question -1]){
                echo
                '<div style="color:red">
                    <input type="radio" id="" name="' . $question . '" value="' . $answer . '" checked>
                    <label for="">' . $value_ans . '</label>
                </div>';
            }else{
                if ($answer == $keys[$question]){
                    echo
                    '<div style="color:green">
                        <input type="radio" id="" name="' . $question . '" value="' . $answer . '">
                        <label for="">' . $value_ans . '</label>
                    </div>';  
                }
                else{
                    echo
                    '<div>
                        <input type="radio" id="" name="' . $question . '" value="' . $answer . '">
                        <label for="">' . $value_ans . '</label>
                    </div>';
                }
            }
        };
        echo
        '</fieldset>';
    };
    // for($i = 0; $i < count($results); $i++) {
    //     if ($results[$i] == $keys[$i+1]) {
    //         echo '<div><h4 style="">'. "Câu " . $i+1 . ": " . strtoupper($results[$i]) . '</h4></div>';
    //         $point += 1;
    //     }else{
    //         if ($results[$i] == "0") {
    //             echo '<div><h4 style="color:blue">'. "Câu " . $i +1 . ": Chưa chọn đáp án". '</h4></div>';
    //         }else{
    //             echo '<div><h4 style="color:red">' . "Câu " . $i+1 . ": " . strtoupper($results[$i]) .'</h4></div>';
    //         }
    //     }
    // }
}

echo '<div><h2>' . "Tổng điểm của bạn là: " . $point ."/10.". '</div>';
if($point<4){
    echo '<div><h2>' . "Nhận xét: Bạn quá kém, cần ôn tập thêm.".'</div>';
}else if($point<=7){
    echo '<div><h2>' . "Nhận xét: Cũng bình thường.".'</div>';
}else{
    echo '<div><h2>' . "Nhận xét: Sắp sửa làm được trợ giảng lớp PHP.".'</div>';
}

?>