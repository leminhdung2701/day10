<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="css.css" type="text/css">

<div>
    <h1>TRẮC NGHIỆM</h1>
</div>

<?php
$questions = array(
    1 => "Xe máy nặng bao nhiêu cân?",
    2 => "Năm nay năm bao nhiêu?",
    3 => "Bạn Duy thích học môn nào nhất?",
    4 => "Bạn Duy nghiện gì?",
    5 => "Khi nào K64 tốt nghiệp?",

    6 => "Xe máy nặng bao nhiêu cân?",
    7 => "Năm nay năm bao nhiêu?",
    8 => "Bạn Duy thích học môn nào nhất?",
    9 => "Bạn Duy nghiện gì?",
    10 => "Khi nào K64 tốt nghiệp?",
);

$answers = array(
    array("a" => "100 kg", "b" => "10 yến", "c" => "1 tạ", "d" => "0,1 tấn"),
    array("a" => "2010", "b" => "2022", "c" => "2023", "d" => "2011"),
    array("a" => "Lập trình Web", "b" => "Học máy", "c" => "Thị giác máy tính", "d" => "Giải tích số"),
    array("a" => "Bùng học", "b" => "LOL", "c" => "CSGO", "d" => "Dota2"),
    array("a" => "01/2023", "b" => "12/2022", "c" => "01/2024", "d" => "08/2023"),

    array("a" => "100 kg", "b" => "10 yến", "c" => "1 tạ", "d" => "0,1 tấn"),
    array("a" => "2010", "b" => "2022", "c" => "2023", "d" => "2011"),
    array("a" => "Lập trình Web", "b" => "Học máy", "c" => "Thị giác máy tính", "d" => "Giải tích số"),
    array("a" => "Bùng học", "b" => "LOL", "c" => "CSGO", "d" => "Dota2"),
    array("a" => "01/2023", "b" => "12/2022", "c" => "01/2024", "d" => "08/2023"),
);

$data = array();
if (!empty($_POST["submit"])) {
    for ($i = 1; $i <= 5; $i++) {
        $data[$i] = (isset($_POST[$i]))? $_POST[$i]:"0";
    }
    setcookie('cookie_ans_1' , json_encode($data),time() + (86400 * 30)); // 86400 = 1 day    
    header('Location: nextpage.php');
}

?>

<form method="POST" action="index.php" enctype='multipart/form-data'>


    <?php
    foreach ($questions as $question => $value) {
        if ($question <= 5) {
            echo
            '<fieldset>
            <legend>Câu ' . $question . ':</legend>
            <div>
                    ' . $value . '
            </div>';
            foreach ($answers[$question - 1] as $answer => $value_ans) {
                echo
                '<div>
                    <input type="radio" id="" name="' . $question . '" value="' . $answer . '">
                    <label for="">' . $value_ans . '</label>
                </div>
            ';
            };
            echo
            '</fieldset>';
        }
    };
    ?>


    <div>
        <input class="next" type="submit" value="Next &raquo;" name="submit">
    </div>

</form>